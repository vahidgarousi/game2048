package framework.core;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.TransitionRes;

public class VBase extends Application {
  private static Context context;
  private static Activity currentActivity;
  private static LayoutInflater layoutInflater;
  private static TransitionInflater transitionInflater;
  private static Handler handler;
  private static DisplayMetrics displayMetrics;
  private static VBase base;
  private static final String TAG = "Logger";

  public static void setCurrentActivity(Activity activity) {
    currentActivity = activity;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Log.i(TAG, "onCreate: Hello from VBase");
    context = getApplicationContext();
    layoutInflater = LayoutInflater.from(context);
    handler = new Handler();
    displayMetrics = getResources().getDisplayMetrics();
    base = this;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      transitionInflater = TransitionInflater.from(context);
    }

  }

  public static Context getContext() {
    if (currentActivity != null) {
      return currentActivity;
    }
    return context;
  }

  public static LayoutInflater getLayoutInflater() {
    return layoutInflater;
  }

  public static View layoutInflate(@LayoutRes int res) {
    return layoutInflater.inflate(res, null);
  }

  public static View layoutInflate(@LayoutRes int res, @Nullable ViewGroup root) {
    return layoutInflater.inflate(res, root);
  }

  public static Transition inflateTransition(@TransitionRes int res) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      return transitionInflater.inflateTransition(res);
    }
    return null;
  }

  public static TransitionInflater getTransitionInflater() {
    return transitionInflater;
  }

  public static Activity getCurrentActivity() {
    return currentActivity;
  }

  public static Handler getHandler() {
    return handler;
  }

  public static DisplayMetrics getDisplayMetrics() {
    return displayMetrics;
  }

  public static VBase get() {
    return base;
  }
}
