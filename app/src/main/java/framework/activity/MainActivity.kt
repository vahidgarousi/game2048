package framework.activity

import android.os.Bundle
import ir.vahidgarousi.app.game248.R

class MainActivity : VAppCompatActivity() {

    inner class Ui {

    }

    private lateinit var ui: Ui
    override fun onCreate(savedInstanceState: Bundle?) {
        ui = Ui()
        super.onCreate(savedInstanceState)
        Founder(this)
            .noActionbar()
            .noTitleBar()
            .fullscreen()
            .contentView(R.layout.activity_main)
            .extractUi(ui)
            .build()
    }
}
