package framework.custom_view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.Toast
import framework.core.VBase

class GameView : ImageView {
    private lateinit var paint: Paint
    private lateinit var textPaint: Paint
    private var width: Int? = 0
    private var height: Int? = 0
    private val cells = Array(4, { IntArray(4) })

    init {
        initialize()
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private fun initialize() {
        paint = Paint()
        paint.color = Color.parseColor("#aaaaaa")
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.isAntiAlias = true
        textPaint = Paint()
        textPaint.color = Color.parseColor("#000000")
        textPaint.style = Paint.Style.FILL
        textPaint.textSize = 80F
        textPaint.textAlign = Paint.Align.CENTER
        textPaint.isAntiAlias = true
        resetGame()
    }

    private fun resetGame() {
        for (i in 0 until 4) {
            for (j in 0 until 4) {
                cells[i][j] = 0
            }
        }
        dropNextNumber()
        dropNextNumber()
        test1()
    }


    private fun dropNextNumber() {
        var gameFinished: Boolean = true
        var freeCells = 0
        for (i in 0 until 4) {
            for (j in 0 until 4) {
                if (cells[i][j] == 0) {
                    freeCells++
                    if (freeCells > 1) {
                        gameFinished = false
                        break
                    }
                }
            }
        }
        if (!gameFinished) {
            while (true) {
                val randomX = Math.floor(Math.random() * 4).toInt()
                val randomY = Math.floor(Math.random() * 4).toInt()
                if (cells[randomX][randomY] == 0) {
                    cells[randomX][randomY] = 2
                    break
                }
            }
        }
        invalidate()
        if (gameFinished) {
            Toast.makeText(VBase.getContext(), "You Loose", Toast.LENGTH_LONG).show()
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        width = getWidth()
        height = getHeight()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (canvas != null) {
            val size = width?.div(4)!!.toInt()
            val padding = 5
            for (i in 0 until 4) {
                for (j in 0 until 4) {
                    val rect =
                        Rect(j * size + padding, i * size + padding, (j + 1) * size - padding, (i + 1) * size - padding)
                    canvas.drawRect(rect, paint)
                    if (cells[i][j] != 0) {
                        canvas.drawText(
                            cells[i][j].toString(),
                            ((j + 0.5) * size).toFloat(),
                            ((i + 0.5) * size).toFloat(),
                            textPaint
                        )
                    }
                }
            }
        }
    }


    private fun test1() {
        for (i in 0 until 4) {
            for (j in 0 until 4) {
                cells[i][j] = 0
            }
        }
        cells[0][1] = 2
        cells[0][2] = 4
        cells[1][0] = 2
        cells[1][3] = 2
        cells[3][3] = 8
        cells[2][1] = 2
        cells[3][2] = 2
        cells[3][0] = 8
        goToRight()
    }

    private fun goToLeft() {
        /*
         *      2   4
         *  2           2
         *      2
         *              8
         */
        /*
        *  2    4
        *  4
        *  2
        *  8
        *
        * */
        for (i in 0 until 4) {
            for (j in 0 until 4) {
                if (j > 0) {
                    var k = j
                    while (k > 0 && cells[i][k - 1] == 0) {
                        cells[i][k - 1] = cells[i][k]
                        cells[i][k] = 0
                        k--
                    }
                    if (k > 0) {
                        if (cells[i][k - 1] == cells[i][k]) {
                            cells[i][k - 1] *= 2
                            cells[i][k] = 0
                        }
                    }
                }
            }
        }
        invalidate()
    }

    private fun goToRight() {
        /*
         *      2   4
         *  2           2
         *      2
         *              8
         */
        /*
        *            2   4
        *                4
        *                2
        *                8
        *
        * */
        for (i in 0 until 4) {
            for (j in 0 until 4) {
                if (j < 3) {
                    var k = j
                    while (k < 3 && cells[i][k + 1] == 0) {
                        cells[i][k + 1] = cells[i][k]
                        cells[i][k] = 0
                        k++
                    }
                    if (k < 3) {
                        if (cells[i][k + 1] == cells[i][k]) {
                            cells[i][k + 1] *= 2
                            cells[i][k] = 0
                        }
                    }
                }
            }
        }
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                dropNextNumber()
            }
        }
        return true
    }
}